def berechnungsTypFunktion():
    while True:
        try:
            berechnungsTyp = input("Möchtest du über Strecke(D) oder über Zeit(T) berechnen?\n")[0]
            if berechnungsTyp in "dD":
                print("Strecke ")
                return "d"
                break

            elif berechnungsTyp in "tT":
                print("Zeit ")
                return "t"
                break
            else:
                print("Bitte eien gültige eingabe...")

        except:
            print("Bitte eien gültige eingabe!")


def berechnungsFunktion(x):
    if x == "d":
        while True:
            try:
                strecke = int(input("Wie weit möchtest du Fahren (in Km)? \n"))
                if(strecke >= 0):
                    break
                else:
                    print("Keine negativen Werte!")
            except:
                print("Ungültige Eingabe!")
        preis = float(strecke) * 0.75
    elif x == "t":
        while True:
            try:
                zeit = int(input("Wie lange möchtest du Fahren (in Minuten)? \n"))
                if (zeit >= 0):
                    break
                else:
                    print("Keine negativen Werte!")
            except:
                print("Ungültige Eingabe!")
        preis = float(zeit) * 0.25
    return preis


if __name__ == '__main__':
    print("Es wird " + str(berechnungsFunktion(berechnungsTypFunktion())) + "€ kosten.")
    input()
